# Node 15
FROM d2850632b602 as dependencies

WORKDIR /app
COPY package*.json ./
RUN npm config set fetch-retry-maxtimeout 60000 -g && npm install

# Node 15 Alpine
FROM f201b1d5d878
WORKDIR /app
COPY --from=dependencies /app ./
COPY . ./
CMD ["npm", "run", "start"]
